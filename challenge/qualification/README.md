# AI Team Qualification Challenge

First of all, thanks for spending some time reading this README file. We believe at the time you found this repo, you already have programming skills (Scratch, Blockly and drag-and-drop codes are excluded), everyone can use ANY programming language to join the AI Team.

## ML Introduction
Machine learning (ML) is the scientific study of algorithms and statistical models that computer systems use to perform a specific task without using explicit instructions, relying on patterns and inference instead. It is seen as a subset of artificial intelligence. Machine learning algorithms build a mathematical model based on sample data, known as "training data", in order to make predictions or decisions without being explicitly programmed to perform the task. Machine learning algorithms are used in a wide variety of applications, such as email filtering and computer vision, where it is difficult or infeasible to develop a conventional algorithm for effectively performing the task. 

## Challenge Introduction
In the current challenge, we will be doing something related to Machine Learning, and it is also the most important step in every machine learning related projects - **Data Collection**.

Data collection is the process of gathering and measuring information from countless different sources. In order to use the data we collect to develop practical artificial intelligence (AI) and machine learning solutions, it must be collected and stored in a way that makes sense for the business problem at hand.

## Problem description
2019 Novel Coronavirus (2019-nCoV) is a virus (more specifically, a coronavirus) identified as the cause of an outbreak of respiratory illness first detected in Wuhan, China. Early on, many of the patients in the outbreak in Wuhan, China reportedly had some link to a large seafood and animal market, suggesting animal-to-person spread. However, a growing number of patients reportedly have not had exposure to animal markets, indicating person-to-person spread is occurring. At this time, it’s unclear how easily or sustainably this virus is spreading between people.

As a data scientist, you're assign with a task to gather and combine some datasets in order to use it to train your machine learning model. You found some datasets that might be useful after surfing the Internet (located in the `datasets/`). 

## Input data set
In `datasets/`, `ncov_confirmed.csv` contains time series data of confirmed cases, `ncov_deaths.csv` contains time series data of death cases and `ncov_recovered.csv` contains time series data of recovered cases. Each file mentioned above contains `province/state`, `country/region`, `Lat`, `Long`, and also a few dates as field column.

## Your work
Using any programming language, extract and combine all the data so the output data looks something like this.

| Date          | Province/State | Country/Region | Confirmed      | Deaths         | Recovered      |
| ------------- | -------------- | -------------- | -------------- | -------------- | -------------- |
| 01/22/2020    | Anhui          | China          | 1              | 0              | 0              |
| 01/22/2020    | Beijing        | China          | 14             | 0              | 0              |
| 01/23/2020    | Anhui          | China          | 9              | 0              | 0              |
| 01/23/2020    | Beijing        | China          | 22             | 0              | 0              |

First column contains the date, second column is the province/state, third column is country/region, fourth column shows the confirmed cases number, fifth column shows the death cases number, and the last column shows the recovered cases number.

For every row, for example if the date is 01/22/2020, and the province is Anhui, extract all the data related to that specific date and province/state from each file, showing the number of confirmed cases, deaths cases and recovered cases.
Loop until all the province/state's data already being extracted your new output file, then do the same thing for the following date, until everything is extracted from the three unprocessed datasets.

To make it easier, generate the output data so it has the format(date,province/state,country/region,confirmed,deaths,recovered) like this in a `.txt` file.
```
01/22/2020, Anhui, China, 1, 0, 0
01/22/2020, Beijing, China, 14, 0, 0
......
......
......
```

## Submissions
After generating the output file, create a GitLab repo(Google it if you don't know how) and push your code including your output file into that repo, then send the repo URL using Discord DM to `@genesis331#1392`. Don't forget to make sure that your repo can be viewed publicly!
